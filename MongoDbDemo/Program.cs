﻿using System;

namespace MongoDbDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoPresentation.RunDemo();

            Console.ReadKey();
        }
    }
}

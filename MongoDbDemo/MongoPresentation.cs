﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace MongoDbDemo
{
    class MongoPresentation
    {
        /// <summary>
        /// Run MongoDb Driver Demo
        /// </summary>
        public static void RunDemo()
        {
            // строка подключения
            string connectionString = "mongodb://localhost:27017/test";
            var connection = new MongoUrlBuilder(connectionString);
            // получаем клиента для взаимодействия с базой данных
            var client = new MongoClient(connectionString);

            // получаем доступ к самой базе данных
            var database = client.GetDatabase(connection.DatabaseName);

            try
            {
                WorkWithBsonDocument(database);

                WorkWithEntityPerson(database);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception mesage: " + e.Message);
            }
            finally
            {
                client.DropDatabase(connection.DatabaseName);
                Console.WriteLine($"Database {connection.DatabaseName} dropped");
            }
        }

        /// <summary>
        /// Work with collection as with BSON document
        /// </summary>
        /// <param name="database">Mongo database</param>
        private static void WorkWithBsonDocument(IMongoDatabase database)
        {
            // Пытаемся иль достать существующую, иль создаем новую коллекцию
            Console.WriteLine("Create new collection users");
            var usersCollection = database.GetCollection<BsonDocument>("users");

            PrintAmountOfDocs(usersCollection);

            // Производим вставку пользователя в коллекцию
            Console.WriteLine("\nInsert first item\n");
            usersCollection.InsertOne(new BsonDocument { { "name", "Tom" }, { "age", new BsonInt32(12) } });

            PrintAmountOfDocs(usersCollection);

            // Один из вариантом маппинга
            BsonClassMap.RegisterClassMap<Person>(x =>
            {
                x.AutoMap();
                x.MapMember(p => p.Name).SetElementName("name");
            });

            // Инициализируем пользователя
            var user = new Person
            {
                Name = "Tetiana",
                Age = 27
            };

            Console.WriteLine("\nInsert second item\n");
            usersCollection.InsertOne(user.ToBsonDocument());

            PrintAmountOfDocs(usersCollection);

            Console.WriteLine("\nInsert many items\n");
            usersCollection.InsertMany(GetMockedUsers(50));
            PrintAmountOfDocs(usersCollection);

            // Достаем по селектору пользователей из базы при помощи возможностей MongoDriver
            var filter = Builders<BsonDocument>.Filter.Eq("name", "Tom");

            var users = usersCollection.Find<BsonDocument>(filter).ToList();

            Console.WriteLine($"\nAfter filtration using Builder by selector name equal 'Tom':\n");
            foreach (var item in users)
            {
                Console.WriteLine($"Name: {item["name"]} Age: {item["age"]}");
            }

            Console.WriteLine("------------------------------------\n");
        }

        /// <summary>
        /// Work with documents as collection of Persone
        /// </summary>
        /// <param name="database">MongoDb database</param>
        private static void WorkWithEntityPerson(IMongoDatabase database)
        {
            // Пытаемся иль достать существующую, иль создаем новую коллекцию
            Console.WriteLine("Create new collection people");
            var peopleCollection = database.GetCollection<Person>("people");
            PrintAmountOfDocs(peopleCollection);

            // Инициализируем пользователя
            var user = new Person
            {
                Name = "Tetiana",
                Age = 27
            };

            // Производим вставку пользователя в коллекцию
            peopleCollection.InsertOne(user);
            peopleCollection.InsertMany(GetMockedPeople(50));

            // Достаем по селектору пользователей из базы при помощи Linq
            var result = peopleCollection.AsQueryable().Where(x => x.Age > 15).ToList();

            Console.WriteLine("\nAfter filtration with Linq:\n");
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("------------------------------------\n");

            // Особенность Linq to MongoDb, First и FirstOrDefault не принимает лямбду для фильтрации
            var userToReplace = peopleCollection.AsQueryable().Where(x => x.Name == "Tom").FirstOrDefault();
            Console.WriteLine("\nBefore replace:\n");
            Console.WriteLine(userToReplace);

            peopleCollection.ReplaceOne(x => x.Id == userToReplace.Id, new Person { Id = userToReplace.Id, Name = "NewName", Age = 33 });

            // Достаем по селектору пользователей из базы при помощи Linq
            result = peopleCollection.AsQueryable().Where(x => x.Name == "NewName").ToList();
            Console.WriteLine("\nAfter replace:\n");
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("------------------------------------\n");

            PrintAmountOfDocs(peopleCollection);

            // Подготавливаем данные для модификации пользователей

            Console.WriteLine("\nBefore Update all users with name Tom:\n");
            result = peopleCollection.AsQueryable().Where(x => x.Name == "Tom").ToList();
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            var personeUpdateDefinition = Builders<Person>.Update.Set(person => person.Age, 99);
            peopleCollection.UpdateMany(person => person.Name == "Tom", personeUpdateDefinition);

            result = peopleCollection.AsQueryable().Where(x => x.Name == "Tom").ToList();
            Console.WriteLine("\nAfter Update all users with name Tom:\n");
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        /// <summary>
        /// Write to console amount of document in choosen collection
        /// </summary>
        /// <param name="collection">MongoDb collection for print</param>
        private static void PrintAmountOfDocs<T>(IMongoCollection<T> collection)
        {
            Console.WriteLine($"Amount of items in {collection.CollectionNamespace}: " + collection.Count(new BsonDocument()));
        }

        /// <summary>
        /// Return mocked list of BsonDocuments
        /// </summary>
        /// <param name="countOfUsers">Amount of necessary users</param>
        /// <returns></returns>
        private static IEnumerable<BsonDocument> GetMockedUsers(int countOfUsers)
        {
            var people = GeneratePersons(countOfUsers);
            return people.Select(x => x.ToBsonDocument());
        }

        /// <summary>
        /// Return mocked list of people
        /// </summary>
        /// <param name="countOfUsers">Amount of necessary people</param>
        /// <returns></returns>
        private static IEnumerable<Person> GetMockedPeople(int countOfUsers)
        {
            return GeneratePersons(countOfUsers);
        }

        /// <summary>
        /// Generate specified amount of persons
        /// </summary>
        /// <param name="countOfUsers"></param>
        /// <returns></returns>
        private static IEnumerable<Person> GeneratePersons(int countOfUsers)
        {
            var names = new[] { "Tom", "Sara", "Mike", "Zoya", "Masha", "Sasha", "Artur", "Mikki", "Tonya", "Boris" };
            var rand = new Random(33);
            var result = new List<Person>();
            for (var i = 0; i < countOfUsers; i++)
            {
                var age = rand.Next(10, 99);
                var nameIndex = rand.Next(names.Length);
                var persone = new Person
                {
                    Age = age,
                    Name = names[nameIndex]
                };

                result.Add(persone);
            }

            return result;
        }
    }
}

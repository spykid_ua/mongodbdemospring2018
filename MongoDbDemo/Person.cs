﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDbDemo
{
    public class Person
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonRepresentation(BsonType.String)]
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonRepresentation(BsonType.Int32)]
        [BsonElement("age")]
        public int Age { get; set; }

        public override string ToString()
        {
            return $"Name: {this.Name} Age: {this.Age}";
        }
    }
}